#!/bin/bash

set -ex
cd /docker/eestecnet
python manage.py makemigrations
python manage.py migrate
python manage.py collectstatic --noinput
gunicorn -w 8 --timeout=120 --env DJANGO_SETTINGS_MODULE=eestecnet.settings.deployment eestecnet.wsgi --forwarded-allow-ips="*" -b 0.0.0.0:8080
